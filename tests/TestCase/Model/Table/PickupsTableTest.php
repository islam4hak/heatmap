<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PickupsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PickupsTable Test Case
 */
class PickupsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PickupsTable
     */
    protected $Pickups;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Pickups',
        'app.Users',
        'app.Locations',
        'app.Actions',
        'app.Orders',
        'app.Transactions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pickups') ? [] : ['className' => PickupsTable::class];
        $this->Pickups = TableRegistry::getTableLocator()->get('Pickups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Pickups);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
