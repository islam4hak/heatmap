<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * ReportsUsers component
 */
class ReportsUsersComponent extends ReportsComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->setTheTargetModel('Users');
    }

    public function get_users_count(){
        return $this->get_count([]);
    }
    public function get_active_users_count(){
        $this->setTheTargetModel('Orders');
        return $this->model->find()->where([
            'created >' =>$this->getDate(7)
        ])->group(['Orders.user_id'])->count();
    }
    public function get_today_users_usage(){
        $this->setTheTargetModel('Orders');
        return $this->model->find()->where([
            'created >' =>$this->getDate(1)
        ])->group(['Orders.user_id'])->count();
    }
    public function get_user_register_timeline(){
        $this->setTheTargetModel('Users');

        $duration = 7;
        $all = [
            'count'   =>$this->get_timeline_data('id' , [],'count',null, $duration),
        ];
        $enhanced_return =[];
        foreach ($all['count'] as $date => $value){
            $enhanced_return[$date] = [
                'count'     =>$all['count'][$date],
            ];
        }
        return $enhanced_return ;
    }
    public function get_users_trade_levels(){
        $this->setTheTargetModel('Orders');
        $query = $this->model->find()->contain(['Users'])->select(['total_cod' => 'SUM(Orders.cod)',
            'perc_cod' => '(COUNT(*) / (SELECT COUNT(*) FROM orders)) * 100',
                'username'=>'Users.username'])
            ->group(['Orders.user_id'])->order(['total_cod'=>'DESC'])->map(function($row){
                return [
                    'Brand' => $row['username'],
                    'Total' => $row['total_cod'],
                    'Avg'   => $row['perc_cod'],

                ];
            })->countBy(function ($row) {
                if($row['Avg'] >= 10){ $type = 'Large'; }
                if($row['Avg'] >= 5 && $row['Avg'] < 10){ $type = 'Medium';}
                if($row['Avg'] < 5 ){ $type = 'Small';}
                return $type ;
            }) ;
        $enhanced_return = [];
        foreach ($query->toArray()  as $type => $count) {
            $enhanced_return[] = ['Type' => $type , 'count' => $count];
        }

        return $enhanced_return;
    }
    public function get_best_users(){
        $this->setTheTargetModel('Orders');
        $query = $this->model->find()->contain(['Users'])->select([
            'total_cod' => 'SUM(Orders.cod)',
            'total_orders_count' => 'count(Orders.cod)',
            'perc_cod' => '(COUNT(*) / (SELECT COUNT(*) FROM orders)) * 100',
                'username'=>'Users.username'])
            ->group(['Orders.user_id'])->order(['total_cod'=>'DESC'])->map(function($row){
                return [
                    'Brand' => $row['username'],
                    'Total_cod' => $row['total_cod'],
                    'Percent_of_total_orders'   => $row['perc_cod'],
                    'total_orders_count'   => $row['total_orders_count'],

                ];
            });

        return $query->toArray();
    }
    public function get_inactive_users_count(){
        $this->setTheTargetModel('Users');
        $equation = $this->get_users_count() - $this->get_active_users_count();
        return $equation ;
    }
    public function get_users_list_full_data(){
        return [
            'all_users_count'   => $this->get_users_count(),
            'all_users_active'   => $this->get_active_users_count(),
            'all_users_last_day'   => $this->get_today_users_usage(),
            'all_users_inactive'   => $this->get_inactive_users_count(),
        ];
    }



    /**
     * @param $days
     * @return false|string
     */
    public function getDate($days)
    {
        return date('Y-m-d', (time() - (86400 * $days)));
    }
}
