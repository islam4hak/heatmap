<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * ReportsInvoices component
 */
class ReportsInvoicesComponent extends ReportsComponent
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public function __construct(ComponentRegistry $registry, array $config = [])
    {
        parent::__construct($registry, $config);
        $this->setTheTargetModel('Invoices') ;
    }

    function get_all_invoice_count_per_user($userID){
        return $this->get_count(['user_id' =>$userID]);
    }
    function get_all_invoices_avg_per_user($userId){
//        return $this->get_avg_of_field('')
    }
    function get_full_invoices_data($userId){
        return [
            'total_invoices_count' => $this->get_count(['user_id' =>$userId]),
            'total_invoices_amount_sum' => $this->get_sum_of_field('total_amount' , ['user_id' =>$userId]),
            'total_invoices_amount_avg' => $this->get_avg_of_field('total_amount' , ['user_id' =>$userId]),
            'total_invoices_payout_sum' => $this->get_sum_of_field('total_payout' , ['user_id' =>$userId]),
            'total_invoices_payout_avg' => $this->get_avg_of_field('total_payout' , ['user_id' =>$userId]),
            'total_invoices_fees_sum'   => $this->get_sum_of_field('total_fees' , ['user_id' =>$userId]),
            'total_invoices_fees_avg'   => $this->get_avg_of_field('total_fees' , ['user_id' =>$userId]),
            'total_invoices_orders_sum'   => $this->get_sum_of_field('number_of_orders' , ['user_id' =>$userId]),
            'total_invoices_orders_avg'   => $this->get_avg_of_field('number_of_orders' , ['user_id' =>$userId]),
        ];
    }

    function get_invoices_timeline_per_user($userId=0, $duration=7){
        $conditions = $userId != 0 ? ['user_id' => $userId] : [];
        $conditions['cleared_at IS NOT ']=null;
        $all = [
            'sum'   =>$this->get_timeline_data('total_amount' , $conditions,'sum',null, $duration,'cleared_at'),
            'avg'   =>$this->get_timeline_data('total_amount' , $conditions,'avg',null, $duration,'cleared_at'),
            'count'   =>$this->get_timeline_data('total_amount' , $conditions,'count',null, $duration,'cleared_at'),
        ];
        $enhanced_return =[];
        if(!empty($all['sum'])){
            foreach ($all['sum'] as $date => $value){
                $enhanced_return[$date] = [
                    'sum'       =>$value,
                    'avg'       =>$all['avg'][$date],
                    'count'     =>$all['count'][$date],
                ];
            }
        }

        return $enhanced_return ;
    }

    function get_orders_timeline_per_user($userId){
        $this->setTheTargetModel('Orders');
        $all = [
            'sum'       =>$this->get_timeline_data('cod' ,['user_id' =>$userId],'sum',null,7,'created'),
            'avg'       =>$this->get_timeline_data('cod' ,['user_id' =>$userId],'avg',null,7,'created'),
            'count'     =>$this->get_timeline_data('cod' ,['user_id' =>$userId],'count',null,7,'created'),
        ];

        $enhanced_return =[];
        foreach ($all['sum'] as $date => $value){
            $enhanced_return[$date] = [
                'sum'       =>$value,
                'avg'       =>$all['avg'][$date],
                'count'     =>$all['count'][$date],
            ];
        }
        return $enhanced_return ;    }

        public function get_open_invoices_orders($userId = 0){
            $this->setTheTargetModel('Invoices');
            $query = $this->model->find()->where(['cleared_at IS' => null])->contain(['Orders'])->toArray();
            $count = 0 ;
            foreach ($query as $item) {
                $count += count($item->orders);
            }
            return $count;
        }
}
