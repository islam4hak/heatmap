<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Halls Controller
 *
 * @property \App\Model\Table\HallsTable $Halls
 *
 * @method \App\Model\Entity\Hall[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HallsController extends AppController
{

    /**
     * Sensors Method
     * Description : Method for setting the sensor locations on floor plan
     * @param $id
     */
    function sensors($id){
        $hall = $this->Halls->get($id);

        $this->set('hall', $hall);
        if ($this->request->is('post')) {
            $uploadPath = ROOT.DS.'webroot'.DS.'uploads/';
            $data =  $this->request->getData();
            if(!empty($data)){
                foreach ($data as $sens => $row){
                    $entity = $this->Halls->Sensors->newEmptyEntity();
                    $entity->location_x = $row['x']*100;
                    $entity->location_y = $row['y']*100;
                    $entity->hall_id = $id;
                    $entity->event_id = $hall->event_id;
                    $entity->name = $sens;
                    $fileObj = $row['data'];
                    $this->Halls->Sensors->save($entity);
                    mkdir($uploadPath.DS.$entity->id);
                    $destination = $uploadPath.DS.$entity->id.DS.'data.txt';
                    $fileObj->moveTo($destination);

                }
                $this->Flash->success(__('The Sensors has been saved.'));
                return $this->redirect(['action' => 'index','controller'=>'sensors']);
            }
//            $hall = $this->Halls->patchEntity($hall, $this->request->getData());
//            if ($this->Halls->save($hall)) {
//                $this->Flash->success(__('The hall has been saved.'));
//
//                return $this->redirect(['action' => 'index']);
//            }
//            $this->Flash->error(__('The hall could not be saved. Please, try again.'));
        }
    }

    /**
     * Sensors Method
     * Description : Method for setting the sensor locations on floor plan
     * @param $id
     */
    function analysis($id){
        $hall = $this->Halls->get($id);
        $this->set('hall', $hall);
        $this->set('sensores', $this->Halls->Sensors->find('all')->where(['hall_id'=>$id]));


    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Events'],
        ];
        $halls = $this->paginate($this->Halls);

        $this->set(compact('halls'));

    }

    /**
     * View method
     *
     * @param string|null $id Hall id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->Halls->Points->get_all_sensors_for_hall($id);
        $start_end_point = $this->Halls->Points->get_start_end_timeStamps() ;
        $avg = $this->Halls->Points->get_all_uniq_macs();

        $uq = $this->Halls->Points->get_hall_average_spent_time();
        $hall = $this->Halls->get($id, [
            'contain' => ['Events', 'Points', 'Sensors'],
        ]);

        $this->set('hall', $hall);
        $this->set('counter',[
            [
                'txt' => 'Sensors' ,
                'value' => count($hall->sensors) ,
            ],
            [
                'txt' => 'Total Points Count' ,
                'value' => $this->Halls->Points->countForTotalSensors ,
            ],
            [
                'txt' => 'Unique' ,
                'value'=> count($avg) ,
            ],
            [
                'txt' => 'Average Time ' ,
                'value' => floor( $uq / 60 ) . " Min" ,
            ]

        ]);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($event_id=null)
    {
        $hall = $this->Halls->newEmptyEntity();
        if ($this->request->is('post')) {
            $hall = $this->Halls->patchEntity($hall, $this->request->getData());
            if ($this->Halls->save($hall)) {
                $this->Flash->success(__('The hall has been saved.'));

                return $this->redirect(['action' => 'sensors','controller'=>'halls',$hall->id]);
            }
            $this->Flash->error(__('The hall could not be saved. Please, try again.'));
        }
        $events = $this->Halls->Events->find('list', ['limit' => 200]);
        $this->set(compact('hall', 'events','event_id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Hall id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $hall = $this->Halls->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hall = $this->Halls->patchEntity($hall, $this->request->getData());
            if ($this->Halls->save($hall)) {
                $this->Flash->success(__('The hall has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The hall could not be saved. Please, try again.'));
        }
        $events = $this->Halls->Events->find('list', ['limit' => 200]);
        $this->set(compact('hall', 'events'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Hall id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $hall = $this->Halls->get($id);
        if ($this->Halls->delete($hall)) {
            $this->Flash->success(__('The hall has been deleted.'));
        } else {
            $this->Flash->error(__('The hall could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
