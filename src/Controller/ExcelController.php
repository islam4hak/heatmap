<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Excel Controller
 *
 *
 * @method \App\Model\Entity\Excel[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ExcelController extends AppController
{
    function reader($file,$type = "list"){
        Configure::write('debug' , false );
        $this->disableAutoRender();
        $targetFile = ROOT.DS.'webroot'.DS."excelsups".DS.$file;
        $spreadsheet = IOFactory::load($targetFile);
        $data = array(1,$spreadsheet->getActiveSheet()->toArray(null,true,true,true));
        $highestRow = $spreadsheet->getActiveSheet() ->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getActiveSheet() ->getHighestColumn(); // e.g 'F'
        $dataOfData = $data[1];
        $dataOfData = $this->filterEmptyValues($dataOfData);
            echo json_encode([
                'file'=>$file,
                'data'=>$this->convertExcelIndexToMainIndexs($dataOfData),
                'values'=>$this->handelHeadersOfArray($this->GetMainIndexes($dataOfData)),
            ]);

    }

    public function createOrders ($file,$userid=1){
//        Configure::write('debug' , false );
        $this->disableAutoRender();
        $targetFile = ROOT.DS.'webroot'.DS."excelsups".DS.$file;
        $spreadsheet = IOFactory::load($targetFile);
        $data = array(1,$spreadsheet->getActiveSheet()->toArray(null,true,true,true));
        $highestRow = $spreadsheet->getActiveSheet() ->getHighestRow(); // e.g. 10
        $highestColumn = $spreadsheet->getActiveSheet() ->getHighestColumn(); // e.g 'F'
        $dataOfData = $data[1];
        $dataOfData = $this->filterEmptyValues($dataOfData);
        $toBeSaved = $this->convertExcelIndexToMainIndexs($dataOfData);
        //saving
        $this->loadModel('Orders');
        foreach ($toBeSaved as $record){
            $name = isset($record['Name'])?$record['Name']:'';
            $phone = isset($record['Phone'])?$record['Phone']:'';
            $address = isset($record['Address'])?$record['Address']:'';
            $city = isset($record['City'])?$record['City']:'Cairo';
            $cod = isset($record['Cost'])?$record['Cost']:0;
            $ref = isset($record['Ref'])?$record['Ref']:"";
            $ent  = $this->Orders->newEmptyEntity();
            $ent->user_id = $userid;
            $ent->receiver_name = $name;
            $ent->receiver_phone = $phone;
            $ent->receiver_address = $address;
            $ent->city= $city ;
            $ent->cod= $cod;
            $ent->reference= $ref;
            $this->Orders->save($ent);
            unset($ent);
        }
        $this->redirect('/orders');
    }



    private function convertExcelIndexToMainIndexs($arr){
        //get main indexes
        $indexes = $this->handelHeadersOfArray($this->GetMainIndexes($arr));
        // remove first row
        unset($arr[1]);
        // assign new index to the old ones
        foreach ($arr as $key => $val){

            foreach($val as $k => $v){
                if(isset($indexes[  $k ])){
                    $newKey = $indexes [ $k ];
                    unset($arr[$key][  $k ]);
                    $arr[$key][  $newKey ] =  $v;
                }
            }
        }
        return $arr;
        // return the new structred array
    }

    private function handelHeadersOfArray($header){
        foreach($header as $key =>$val){
            $val = str_replace(' ','_',$val);
            $header[$key] = $val;
        }
        return $header;
    }
    private function GetMainIndexes($arr){
        return $arr[1];
    }
    private  function filterEmptyValues($arr){
        $returner = [];
        foreach ($arr as $key=> $row){
            $savedRow = [] ;
            foreach($row as $keys => $values ){
                if($values != ""){
                    $savedRow[$keys] = $values;
                }
            }
            $returner[$key] = $savedRow;
        }
        return $returner;
    }

}
