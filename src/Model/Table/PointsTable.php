<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Filesystem\File;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Points Model
 *
 * @property \App\Model\Table\EventsTable&\Cake\ORM\Association\BelongsTo $Events
 * @property \App\Model\Table\SensorsTable&\Cake\ORM\Association\BelongsTo $Sensors
 * @property \App\Model\Table\HallsTable&\Cake\ORM\Association\BelongsTo $Halls
 *
 * @method \App\Model\Entity\Point newEmptyEntity()
 * @method \App\Model\Entity\Point newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Point[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Point get($primaryKey, $options = [])
 * @method \App\Model\Entity\Point findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Point patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Point[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Point|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Point saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Point[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Point[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Point[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Point[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PointsTable extends Table
{

    var $singleSensorArray =[];
    var $fullSensorData =[];
    var $uniqueMacList =[];
    var $countForCurrentSensor =0;
    var $countForTotalSensors =0;
    var $firstRecord = '';
    var $lastRecord= '';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('points');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Events', [
            'foreignKey' => 'event_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Sensors', [
            'foreignKey' => 'sensor_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Halls', [
            'foreignKey' => 'hall_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('mac')
            ->maxLength('mac', 255)
            ->requirePresence('mac', 'create')
            ->notEmptyString('mac');

        $validator
            ->integer('at_time')
            ->requirePresence('at_time', 'create')
            ->notEmptyString('at_time');

        $validator
            ->integer('distance')
            ->requirePresence('distance', 'create')
            ->notEmptyString('distance');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['event_id'], 'Events'));
        $rules->add($rules->existsIn(['sensor_id'], 'Sensors'));
        $rules->add($rules->existsIn(['hall_id'], 'Halls'));

        return $rules;
    }

    public function get_sensors_files($sensorId){
        //cache logic
            /*
             * steps check if the cached file exicet
             * if not back to the old logic
             * add cached to the logic
             */
        $cachedFile = ROOT.DS.'webroot'.DS.'cached'.DS.$sensorId.DS.'data.json';
        if(file_exists($cachedFile)){
            $file = new File($cachedFile);
            $contents = $file->read();
            $arrayOfPoints = json_decode($contents);
            $this->firstRecord=$arrayOfPoints[0];
            $this->singleSensorArray = $arrayOfPoints;
            $this->fullSensorData[$sensorId] = $arrayOfPoints;
            $this->countForCurrentSensor = count($arrayOfPoints);
            $this->countForTotalSensors += count($arrayOfPoints);
        }else {
            $filePath = ROOT . DS . 'webroot' . DS . 'uploads' . DS . $sensorId . DS . 'data.txt';
            $file = new File($filePath);
            $contents = $file->read();
            $arrayOfPoints = explode('
', $contents);
            $this->firstRecord = $arrayOfPoints[0];
            $this->singleSensorArray = $arrayOfPoints;
            $this->fullSensorData[$sensorId] = $arrayOfPoints;
            $this->countForCurrentSensor = count($arrayOfPoints);
            $this->countForTotalSensors += count($arrayOfPoints);
            $this->doCachedSensor($sensorId,$arrayOfPoints);
        }
    }

    private function doCachedSensor($sensorId,$data){
        $cachedFile = ROOT.DS.'webroot'.DS.'cached'.DS.$sensorId.DS.'data.json';
        $file = new File($cachedFile, true, 0644);
        $file->write(json_encode($data));
    }
    private function doCachedFullHallSensors($hall,$fulldata){
        $cachedFile = ROOT.DS.'webroot'.DS.'cached'.DS.'halls'.DS.$hall.DS.'data.json';
        $file = new File($cachedFile, true, 0644);
        $file->write(json_encode($fulldata));
    }

    function get_all_sensors_for_hall($hallId){
        $sensors = TableRegistry::getTableLocator()->get('Sensors');
        $allSensores = $sensors->find('all')->where(['hall_id'   =>$hallId])->toArray();
        $cachedFile = ROOT.DS.'webroot'.DS.'cached'.DS.'halls'.DS.$hallId.DS.'data.json';
        if(file_exists($cachedFile)){
            $file = new File($cachedFile);
            $this->fullSensorData = json_decode($file->read());
        }else{
            foreach ($allSensores as $sensor){
                $this->get_sensors_files($sensor->id);
            }
            $this->doCachedFullHallSensors($hallId,$this->fullSensorData);

        }


    }

    function get_sensor_handel_query($time,$max = 60000,$min=1){

        /*    $finder = $this->Points->find('all')->where([
            'Points.hall_id'   =>$hall,
            'Points.at_time >'   =>$time,
            'Points.at_time <'   =>$time+$max,

        ])->order(['mac'])->contain(['Sensors']);*/
        $sensors = $this->fullSensorData;
        $returnData = [];
        foreach ($sensors as $sensor_id => $data){
            foreach ($data as $row){
                $toArray = explode("|",$row);
                if($toArray[0] > $time && $toArray[0] < $max ){
                    $toArray['sensor'] = $sensor_id;
                    $returnData[] = $toArray;
                    $this->lastRecord = $toArray[0];
                }
            }
        }
        return $returnData;
    }

    function get_all_event_sensors_count($eventId){
        $total_count = 0;
        return $total_count ;
    }

    function get_all_uniq_macs($start = 1605499905 ,$end = 1638316800,$min=1){
        $all_data = $this->get_sensor_handel_query($start , $end ,$min);
        $unique = [];
        $policed = [];
        foreach ($all_data as $row){
            $unique[$row[1]][] = $row;
        }
        usort($all_data, function($a, $b) {
            return $a[0] <=> $b[0];
        });

        foreach ($unique as $mac => $data){
            if(count($data) > 20 && count($data) < 400){
                $policed[$mac]= $data;
            }
        }
        if($min > 0 ){
            $policed =$this->filter_mins($policed,$min);
        }
        $this->uniqueMacList =$policed;
        return $policed;
    }

    function filter_mins($data,$min){
        $return = [];
        $countToCheck = $min * 3;
        foreach ($data as $mac => $row) {
            if (count($row) > $countToCheck) {
                $return[$mac] = $row;
            }
        }

            return $return;
    }
    function handel_mac_array($start = 1605499905 ,$end = 1605659200){
        $all_data = $this->get_sensor_handel_query($start , $end );
        $unique = [];
        foreach ($all_data as $row){
            $unique[$row[1]][] = $row;
        }
        $this->uniqueMacList =$unique;
        return $unique;
    }

    function get_hall_average_spent_time(){
        $list = $this->uniqueMacList;
        $avrges = [];
        $total = 0;
        foreach ($list as $item) {
            if(count($item) >20 ){
                $total +=  count($item)*10;
                $avrges[] = count($item)*10;
            }
        }
        $ret = $total/count($avrges);
        return $ret;
    }

    function get_start_end_timeStamps(){
        $extract = explode('|' , $this->firstRecord);
        $return = [
            'first' => (int) $extract[0],
            'last' =>(int) $this->lastRecord,
            ];
        return $return;
    }


}
