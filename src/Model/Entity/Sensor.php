<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sensor Entity
 *
 * @property int $id
 * @property string $name
 * @property int $location_x
 * @property int $location_y
 * @property int $event_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $hall_id
 *
 * @property \App\Model\Entity\Event $event
 * @property \App\Model\Entity\Hall $hall
 * @property \App\Model\Entity\Point[] $points
 */
class Sensor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'location_x' => true,
        'location_y' => true,
        'event_id' => true,
        'created' => true,
        'modified' => true,
        'hall_id' => true,
        'event' => true,
        'hall' => true,
        'points' => true,
    ];
}
