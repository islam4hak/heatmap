<script src="https://cdnjs.cloudflare.com/ajax/libs/heatmap.js/2.0.0/heatmap.min.js" integrity="sha512-FpvmtV53P/z7yzv1TAIVH7PNz94EKXs5aV6ts/Zi+B/VeGU5Xwo6KIbwpTgKc0d4urD/BtkK50IC9785y68/AA==" crossorigin="anonymous"></script>
<?php
$imageH = $hallData->height * 10;
$imageW = $hallData->width * 10;
?>
<div class="overlay"></div>

<form>
    <div class="row">
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'Start date/time' ,'type'=>'datetime','id'=>'start_datetime', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'End date/time' ,'type'=>'datetime','id'=>'datetime_end', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('mac_address',[ 'label' => 'Search For Mac','id'=>'mac-address' ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php echo $this->Form->control('time_slip',[ 'label' => 'Dwell in minutes ','id'=>'time_lapse','value'=>1,'type'=>'number' ]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('algorithm',[ 'label' => 'Algorithm ','id'=>'algorithm', 'type'=>'select' , 'options' => [
                'Unique',
                'Repeated',
            ] ]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('des_algo',[ 'label' => 'Formula ','id'=>'des_algo', 'type'=>'select' , 'options' => [
                'Old',
                'New',
                'Mixed'
            ] ]); ?>
        </div>

        <!-- <div class="col"> -->
            <?php
            // echo $this->Form->control('square',[ 'label' => 'Square Size in M ','id'=>'square_size','value'=>2.5,'type'=>'number' ]);
             ?>
        <!-- </div> -->
        <div class="col">
            <?php echo $this->Form->control('ignore',[ 'label' => 'Max RSSI','id'=>'ignore_count','value'=>70,'type'=>'number' ]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('base_rssi',[ 'label' => 'Base RSSI ','id'=>'base_rssi','value'=>-46,'type'=>'number' ]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('env_factor',[ 'label' => 'Env Factor','id'=>'env_factor','value'=>25,'type'=>'number' ]); ?>
        </div>



        <div class="col">
            <?php echo $this->Form->control('density',[ 'label' => 'Visualization Density','id'=>'map_density','value'=>1,'type'=>'number' ]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('opacity',[ 'label' => 'Opacity','id'=>'opacity','value'=>60, 'max'=>'99','min'=>'1','type'=>'number' ]); ?>
        </div>
        <div class="col">
            <br/>
            <button type="button" id="search_button" class="btn btn-secondary">Search</button>
        </div>
    </div>
</form>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sensor[]|\Cake\Collection\CollectionInterface $sensors
 */







?>
<div class="row">

    <div class="col-xl-3">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">total unique visitors </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="unique-counter">0</span> </span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>

    </div>
    <div class="col-xl-3">

        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Dwell average for selected dates </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="min-count">0</span> Mins</span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>

    </div>

    <div class="col-xl-3">

        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">visitors counter  </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="visitorCount">0</span> Visitor</span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>

    </div>
    <div class="col-xl-3">

        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Total dwell average   </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="dwell-avg">0</span> Mins</span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>

    </div>



</div>
<div class="row">
    <div class="col-xl-12">
        <div class="demo-wrapper">

            <div id='heatMap'></div>
            <div class="tooltipxxxx"></div>

        </div>

    </div>
</div>


<style>
    #heatMap {
        background-image: url(<?=$hallData->floor_image?>);
        height: <?=$imageH?>px;
        width: <?=$imageW?>px;
        background-size:contain ;
    }
    .tooltipxxxx { position:absolute; left:0; top:0; background:rgba(0,0,0,.8); color:white; font-size:14px; padding:5px; line-height:18px; display:none;}
    .demo-wrapper { position:relative; }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<script>

    $(document).ready(function(){


        var heatmapInstance = h337.create({
            // only container is required, the rest will be defaults
            container: document.querySelector('#heatMap'),
            // radius: 10,
            // blur: .75,
            // blur: 0.75,

        });

        $('#search_button').click(function () {
            console.log(delete heatmapInstance);
            console.log(delete points);
            console.log(delete data);

            $time = new Date($('#start_datetime').val()).getTime() / 1000
            $endTime = new Date($('#datetime_end').val()).getTime() / 1000
            $searchedMac = $('#mac-address').val() == '' ? '' : '/'+$('#mac-address').val() +'/';
            checkmac = $('#mac-address').val() == '' ? false : true;
            $value_for_count = parseInt( $('#map_density').val() );
            $time_lapse = $('#time_lapse').val();
            $time_lapse = $('#time_lapse').val();
            $algo = $('#algorithm').val();
            $des = $('#des_algo').val();
            $ignore = $('#ignore_count').val();
            $base_rssi = $('#base_rssi').val();
            $env_factor = $('#env_factor').val();
            $pallet = $('#square_size').val();
            $opacity = $('#opacity').val();
            $zzz = "0."+$opacity;
            $zzz =  parseFloat($zzz);

            heatmapInstance.configure({
                maxOpacity:  $zzz
                });

            if($algo ==2){
                heatmapInstance.configure({
                    radius: 10,
                    blur: .75,
                });

            }
            if($searchedMac.length > 3){
                $urlLinkPath =  "<?=ROOT_URL?>/points/pins/"+<?=$hallData->id?>+"/"+$time+"/"+$endTime+"/"+$time_lapse+"/"+$algo+"/"+$des+"/"+$ignore+"/"+$base_rssi+"/"+"/"+$env_factor+"/"+$pallet+$searchedMac,
                window.open($urlLinkPath);
            }
            $.ajax({
                url: "<?=ROOT_URL?>/points/heatmap_by_time/"+<?=$hallData->id?>+"/"+$time+"/"+$endTime+"/"+$time_lapse+"/"+$algo+"/"+$des+"/"+$ignore+"/"+$base_rssi+"/"+"/"+$env_factor+"/"+$pallet+$searchedMac,
                context: document.body
            }).done(function($jsonData) {
                console.log(heatmapInstance);
                $total_visiros = JSON.parse($jsonData).uniquex;
                $avg_onMap = JSON.parse($jsonData).average;
                $full_avrg = JSON.parse($jsonData).full_average;
                $('#unique-counter').text($total_visiros);
                $('#min-count').text($avg_onMap);
                $jsonData = JSON.parse($jsonData).data;

                // console.log("data amount " + $jsonData.length);
                // $dwllAvg = Math.floor( $jsonData.length / $total_visiros );
                $('#dwell-avg').text($full_avrg);

                /* tooltip code start */
                var demoWrapper = document.querySelector('.demo-wrapper');
                var tooltip = document.querySelector('.tooltipxxxx');
                var visitorCount = document.querySelector('#visitorCount');
                function updateTooltip(x, y, value) {
                    // + 15 for distance to cursor
                    var transl = 'translate(' + (x + 15) + 'px, ' + (y + 15) + 'px)';
                    tooltip.style.webkitTransform = transl;
                    tooltip.innerHTML = Math.floor( ($total_visiros / $avg_onMap )* value*0.7 );

                    visitorCount.innerHTML =  Math.floor( ($total_visiros / $avg_onMap )* value*0.7 );
                };

                demoWrapper.onmousemove = function(ev) {
                    var x = ev.layerX;
                    var y = ev.layerY;
                    // getValueAt gives us the value for a point p(x/y)
                    var value = heatmapInstance.getValueAt({
                        x: x,
                        y: y
                    });
                    tooltip.style.display = 'block';
                    updateTooltip(x, y, value);
                };
// hide tooltip on mouseout
                demoWrapper.onmouseout = function() {
                    tooltip.style.display = 'none';
                };


























                    // now generate some random data
                    var points = [];
                    var max = 99;
                    var width = <?=$imageW?>;
                var height = <?=$imageH?>;
                var len = 200;

                $jsonData.forEach(function(xxx){
                    var val = Math.floor(Math.random()*100);
                    var point = {
                        x: xxx.x,
                        y: xxx.y,
                        value: $value_for_count
                    };
                    points.push(point);
                });
                // heatmap data format
                var data = {
                    max: max,
                    data: points
                };
                // if you have a set of datapoints always use setData instead of addData
                // for data initialization
                heatmapInstance.repaint();

                heatmapInstance.setData(data);
                heatmapInstance.repaint();

                console.log(data);




            });
        })
        $(document).on({
            ajaxStart: function(){
                $("body").addClass("loading");
                $('.overlay').show();
            },
            ajaxStop: function(){
                $("body").removeClass("loading");
                $('.overlay').hide();

            }
        });
    })




</script>
<style>
    .overlay{
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 999;
        background: rgba(255,255,255,0.8) url("<?=ROOT_URL?>/img/45.gif") center no-repeat;
    }
    /* Turn off scrollbar when body element has the loading class */
    body.loading{
        overflow: hidden;
    }
    /* Make spinner image visible when body element has the loading class */
    body.loading .overlay{
        display: block;
    }
</style>
