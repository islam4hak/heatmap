<script src="https://cdnjs.cloudflare.com/ajax/libs/heatmap.js/2.0.0/heatmap.min.js" integrity="sha512-FpvmtV53P/z7yzv1TAIVH7PNz94EKXs5aV6ts/Zi+B/VeGU5Xwo6KIbwpTgKc0d4urD/BtkK50IC9785y68/AA==" crossorigin="anonymous"></script>
<?php
$imageH = $hallData->height * 10;
$imageW = $hallData->width * 10;
?>

<form>
    <div class="row">
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'Start Day' ,'type'=>'datetime','id'=>'start_datetime', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>
        <div class="col">
            <?php echo $this->Form->control('datetime',[ 'label' => 'End Day' ,'type'=>'datetime','id'=>'datetime_end', 'value'=>$start_datetime, 'min'=>$start_datetime, 'max'=>$end_datetime]); ?>
        </div>

        <div class="col">
            <?php echo $this->Form->control('mac_address',[ 'label' => 'Search For Mac','id'=>'mac-address' ]); ?>
        </div>
        <div class="col">
            <br/>
            <button type="button" id="search_button" class="btn btn-secondary">Search</button>
        </div>
    </div>
</form>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sensor[]|\Cake\Collection\CollectionInterface $sensors
 */







?>
<div class="row">
    <div class="col-xl-8">
        <div id='heatMap'></div>
    </div>
    <div class="col-xl-4">
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">total unique visitors </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="unique-counter">0</span> </span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>
        <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Time average </h5>
                        <span class="h2 font-weight-bold mb-0"><span id="min-count">0</span> Mins</span>
                    </div>
                    <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-active-40"></i>
                        </div>
                    </div>
                </div>
                <p class="mt-3 mb-0 text-sm">

                </p>
            </div>
        </div>


    </div>
</div>


<style>
    #heatMap {
        background-image: url(<?=$hallData->floor_image?>);
        height: <?=$imageH?>px;
        width: <?=$imageW?>px;
        background-size:contain ;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<script>

    $(document).ready(function(){
        $('#search_button').click(function () {
            console.log(delete heatmapInstance);
            console.log(delete points);
            console.log(delete data);

            $time = new Date($('#start_datetime').val()).getTime() / 1000
            $endTime = new Date($('#datetime_end').val()).getTime() / 1000
            $searchedMac = $('#mac-address').val() == '' ? '' : '/'+$('#mac-address').val() +'/';
            checkmac = $('#mac-address').val() == '' ? false : true;
            $.ajax({
                url: "<?=ROOT_URL?>/points/heatmap_by_time/"+<?=$hallData->id?>+"/"+$time+"/"+$endTime+$searchedMac,
                context: document.body
            }).done(function($jsonData) {
                $('#unique-counter').text(JSON.parse($jsonData).uniquex);
                $('#min-count').text(JSON.parse($jsonData).average);
                $jsonData = JSON.parse($jsonData).data;
                // console.log($jsonData);
                $value_for_count = $jsonData.length / 5000;
                if(checkmac){
                    $value_for_count = 100;
                    $('#unique-counter').text(1);
                }
                // minimal heatmap instance configuration
                var heatmapInstance = h337.create({
                    // only container is required, the rest will be defaults
                    container: document.querySelector('#heatMap')
                });

                // now generate some random data
                var points = [];
                var max = 99;
                var width = <?=$imageW?>;
                var height = <?=$imageH?>;
                var len = 200;

                $jsonData.forEach(function(xxx){
                    var val = Math.floor(Math.random()*100);
                    var point = {
                        x: xxx.x,
                        y: xxx.y,
                        value: $value_for_count
                    };
                    points.push(point);
                });
                // heatmap data format
                var data = {
                    max: max,
                    data: points
                };
                // if you have a set of datapoints always use setData instead of addData
                // for data initialization
                heatmapInstance.setData(data);
                console.log(data);
                setTimeout(function (){
                    heatmapInstance.repaint();
                },100);



            });
        })
    })




</script>
