<?=$this->element('backend/aragon/header')?>

<body class="bg-default">
<div class="main-content">
    <!-- Page content -->
    <br />
    <br />
    <br />
    <div class="container mt-4 pb-5" >
        <div class="" style="text-align: center">

            <img src="https://www.infosalonsgroup.com/wp-content/uploads/2018/04/INFOSALONS_White_Logo_2.png"
                 class="" style="max-width: 200px; margin:0 auto" />
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary border-0 mb-0">


                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small> sign in with credentials</small>
                        </div>


                        <?= $this->Flash->render() ?>
                        <?= $this->fetch('content'); ?>

                    </div>
                </div>
<!--                <div class="row mt-3">-->
<!--                    <div class="col-6">-->
<!--                        <a href="#" class="text-light"><small>Forgot password?</small></a>-->
<!--                    </div>-->
<!--                    <div class="col-6 text-right">-->
<!--                        <a href="#" class="text-light"><small>Create new account</small></a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>

</div>
</body>
</html>
