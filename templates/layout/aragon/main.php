
<?=$this->element('backend/aragon/header')?>


<body>
<!-- Sidenav -->
<?=$this->element('backend/aragon/left_nav')?>

<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Topnav -->
    <?=$this->element('backend/aragon/topnav')?>

    <!-- Header -->
    <!-- Header -->
    <?=$this->element('backend/aragon/counters')?>

    <!-- Page content -->
    <div class="container-fluied  ">
        <div class="row">
            <?php
            $controllllller  =             $this->request->getParam('controller');
            $chk_action      =             $this->request->getParam('action');
            if($controllllller == 'Events' && $chk_action == 'index'){

            ?>
            <div class="col-xl-12">
                <div class="card bg-default">
                    <div class="card-header bg-transparent">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-light text-uppercase ls-1 mb-1">Overview</h6>
                                <h5 class="h3 text-white mb-0">Events Traffic</h5>
                            </div>
                            <div class="col">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="Mac" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                                            <span class="d-none d-md-block">Month</span>
                                            <span class="d-md-none">M</span>
                                        </a>
                                    </li>
                                    <li class="nav-item" data-toggle="chart" data-target="#chart-sales-dark" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="$" data-suffix="k">
                                        <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                                            <span class="d-none d-md-block">Week</span>
                                            <span class="d-md-none">W</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- Chart -->
                        <div class="chart">
                            <!-- Chart wrapper -->
                            <canvas id="chart-sales-dark" class="chart-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>
<!--            <div class="col-xl-4">-->
<!--                <div class="card">-->
<!--                    <div class="card-header bg-transparent">-->
<!--                        <div class="row align-items-center">-->
<!--                            <div class="col">-->
<!--                                <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>-->
<!--                                <h5 class="h3 mb-0">Total orders</h5>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="card-body">-->
<!--                        <div class="chart">-->
<!--                            <canvas id="chart-bars" class="chart-canvas"></canvas>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">


                    <?= $this->Flash->render() ?>
                    <?= $this->fetch('content'); ?>


                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer class="footer pt-0">
            <div class="row align-items-center justify-content-lg-between">
                <div class="col-lg-6">
                    <div class="copyright text-center text-lg-left text-muted">
<!--                        &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>-->
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="nav nav-footer justify-content-center justify-content-lg-end">

                        <li class="nav-item">
                            <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>


<?=$this->element('backend/aragon/footer_scripts')?>
