<!-- Navbar -->
<nav class="main-header navbar navbar-expand  navbar-dark navbar-navy">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="<?=__('Search')?>" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-language"></i>
            </a>
            <div class="dropdown-menu dropdown-menu dropdown-menu-right" style="min-width: auto!important">
               <?php
               foreach (LANG_LIST as $key => $lang) {
                   echo '
                <a href="'.ROOT_URL.'/utils/translations/changelang/'.$key.'" class="dropdown-item">
                   '.__($lang).'
                </a>
                <div class="dropdown-divider"></div>';
               }
               ?>
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge"><?=count($Notifications_list_array->toArray());?></span>
            </a>
            <?=$this->element('Notifictions.dashboard_header')?>

        </li>
        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                <img src="<?=ROOT_URL?>dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2" alt="User Image">
                <span class="d-none d-md-inline"><?=$_SESSION['Auth']['User']['username']?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <!-- User image -->
                <li class="user-header bg-primary">
                    <img src="<?=ROOT_URL?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">

                    <p>
                        <?=$_SESSION['Auth']['User']['email']?>
                        <small>Member since <?=$_SESSION['Auth']['User']['created']->timeAgoInWords()?></small>
                    </p>
                </li>

                <!-- Menu Footer-->
                <li class="user-footer">
                    <a href="<?=ROOT_URL?>/users-manager/users/editprofile" class="btn btn-default btn-flat"><?=__('Profile')?></a>
                    <a href="<?=ROOT_URL?>/users-manager/users/logout" class="btn btn-default btn-flat float-right"><?=__('Sign out')?></a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
