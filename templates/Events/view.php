<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Event $event
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Event'), ['action' => 'edit', $event->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Event'), ['action' => 'delete', $event->id], ['confirm' => __('Are you sure you want to delete # {0}?', $event->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Events'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Event'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Halls'), ['controller' => 'Halls', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Hall'), ['controller' => 'Halls', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['controller' => 'Points', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Point'), ['controller' => 'Points', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['controller' => 'Sensors', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Sensor'), ['controller' => 'Sensors', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="events view large-9 medium-8 columns content">
    <h3><?= h($event->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($event->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Location') ?></th>
                <td><?= h($event->location) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Logo') ?></th>
                <td><?= h($event->logo) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Images') ?></th>
                <td><?= h($event->images) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($event->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Event Start Day') ?></th>
                <td><?= h($event->event_start_day) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Event End Day') ?></th>
                <td><?= h($event->event_end_day) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($event->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($event->modified) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Halls') ?></h4>
        <?php if (!empty($event->halls)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Height') ?></th>
                    <th scope="col"><?= __('Width') ?></th>
                    <th scope="col"><?= __('Number Of Sensors') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($event->halls as $halls): ?>
                <tr>
                    <td><?= h($halls->id) ?></td>
                    <td><?= h($halls->name) ?></td>
                    <td><?= h($halls->event_id) ?></td>
                    <td><?= h($halls->height) ?></td>
                    <td><?= h($halls->width) ?></td>
                    <td><?= h($halls->number_of_sensors) ?></td>
                    <td><?= h($halls->created) ?></td>
                    <td><?= h($halls->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Halls', 'action' => 'view', $halls->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Halls', 'action' => 'edit', $halls->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Halls', 'action' => 'delete', $halls->id], ['confirm' => __('Are you sure you want to delete # {0}?', $halls->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Points') ?></h4>
        <?php if (!empty($event->points)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Mac') ?></th>
                    <th scope="col"><?= __('At Time') ?></th>
                    <th scope="col"><?= __('Distance') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Sensor Id') ?></th>
                    <th scope="col"><?= __('Hall Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($event->points as $points): ?>
                <tr>
                    <td><?= h($points->id) ?></td>
                    <td><?= h($points->mac) ?></td>
                    <td><?= h($points->at_time) ?></td>
                    <td><?= h($points->distance) ?></td>
                    <td><?= h($points->event_id) ?></td>
                    <td><?= h($points->sensor_id) ?></td>
                    <td><?= h($points->hall_id) ?></td>
                    <td><?= h($points->created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Points', 'action' => 'view', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Points', 'action' => 'edit', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Points', 'action' => 'delete', $points->id], ['confirm' => __('Are you sure you want to delete # {0}?', $points->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sensors') ?></h4>
        <?php if (!empty($event->sensors)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Location X') ?></th>
                    <th scope="col"><?= __('Location Y') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col"><?= __('Hall Id') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($event->sensors as $sensors): ?>
                <tr>
                    <td><?= h($sensors->id) ?></td>
                    <td><?= h($sensors->name) ?></td>
                    <td><?= h($sensors->location_x) ?></td>
                    <td><?= h($sensors->location_y) ?></td>
                    <td><?= h($sensors->event_id) ?></td>
                    <td><?= h($sensors->created) ?></td>
                    <td><?= h($sensors->modified) ?></td>
                    <td><?= h($sensors->hall_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Sensors', 'action' => 'view', $sensors->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Sensors', 'action' => 'edit', $sensors->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Sensors', 'action' => 'delete', $sensors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensors->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
