<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hall[]|\Cake\Collection\CollectionInterface $halls
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('New Hall'), ['action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['controller' => 'Points', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Point'), ['controller' => 'Points', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['controller' => 'Sensors', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Sensor'), ['controller' => 'Sensors', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
        <th scope="col"><?= $this->Paginator->sort('event_id') ?></th>
        <th scope="col"><?= $this->Paginator->sort('floor_image') ?></th>


        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($halls as $hall) : ?>
        <tr>
            <td><?= h($hall->name) ?></td>
            <td><?= $hall->has('event') ? $this->Html->link($hall->event->name, ['controller' => 'Events', 'action' => 'view', $hall->event->id]) : '' ?></td>
            <td><?= $this->Html->image($hall->floor_image,['width'=>150]) ?></td>


            <td class="actions">
                <?= $this->Html->link(__('Cluster'), ['action' => 'cluster','controller'=>'points', $hall->id], ['title' => __('View'), 'class' => 'btn btn-primary']) ?>
                <?= $this->Html->link(__('Sensors'), ['action' => 'sensors', $hall->id], ['title' => __('View'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Analysis'), ['action' => 'analysis', $hall->id], ['title' => __('analysis'), 'class' => 'btn btn-secondary']) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $hall->id], ['title' => __('Edit'), 'class' => 'btn btn-secondary']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('')) ?>
        <?= $this->Paginator->prev('< ' . __('')) ?>
        <?= $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
        <?= $this->Paginator->next(__('') . ' >') ?>
        <?= $this->Paginator->last(__('') . ' >>') ?>
    </ul>
</div>
