<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hall $hall
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Hall'), ['action' => 'edit', $hall->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Hall'), ['action' => 'delete', $hall->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hall->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Halls'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Hall'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Events'), ['controller' => 'Events', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Event'), ['controller' => 'Events', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Points'), ['controller' => 'Points', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Point'), ['controller' => 'Points', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Sensors'), ['controller' => 'Sensors', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Sensor'), ['controller' => 'Sensors', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>



<div class="col-xl-12">
    <div class="card bg-default">
        <div class="card-header bg-transparent">
            <div class="row align-items-center">
                <div class="col">
                    <h6 class="text-light text-uppercase ls-1 mb-1">Overview</h6>
                    <h5 class="h3 text-white mb-0">Events Traffic</h5>
                </div>

            </div>
        </div>
        <div class="card-body">
            <!-- Chart -->
            <div class="chart">
                <!-- Chart wrapper -->
                <canvas id="chart-sales-dark" class="chart-canvas"></canvas>
            </div>
        </div>
    </div>
</div>


<div class="halls view large-9 medium-8 columns content">
    <h3><?= h($hall->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($hall->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Event') ?></th>
                <td><?= $hall->has('event') ? $this->Html->link($hall->event->name, ['controller' => 'Events', 'action' => 'view', $hall->event->id]) : '' ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($hall->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Height') ?></th>
                <td><?= $this->Number->format($hall->height) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Width') ?></th>
                <td><?= $this->Number->format($hall->width) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Number Of Sensors') ?></th>
                <td><?= $this->Number->format($hall->number_of_sensors) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($hall->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($hall->modified) ?></td>
            </tr>

            <tr>
                <th scope="row"><?= __('Floor Image') ?></th>
                <td><?= $this->Html->img($hall->floor_image) ?></td>
            </tr>
        </table>
    </div>
    <div class="related">
        <h4><?= __('Related Points') ?></h4>
        <?php if (!empty($hall->points)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Mac') ?></th>
                    <th scope="col"><?= __('At Time') ?></th>
                    <th scope="col"><?= __('Distance') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Sensor Id') ?></th>
                    <th scope="col"><?= __('Hall Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($hall->points as $points): ?>
                <tr>
                    <td><?= h($points->id) ?></td>
                    <td><?= h($points->mac) ?></td>
                    <td><?= h($points->at_time) ?></td>
                    <td><?= h($points->distance) ?></td>
                    <td><?= h($points->event_id) ?></td>
                    <td><?= h($points->sensor_id) ?></td>
                    <td><?= h($points->hall_id) ?></td>
                    <td><?= h($points->created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Points', 'action' => 'view', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Points', 'action' => 'edit', $points->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Points', 'action' => 'delete', $points->id], ['confirm' => __('Are you sure you want to delete # {0}?', $points->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sensors') ?></h4>
        <?php if (!empty($hall->sensors)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Location X') ?></th>
                    <th scope="col"><?= __('Location Y') ?></th>
                    <th scope="col"><?= __('Event Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col"><?= __('Hall Id') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($hall->sensors as $sensors): ?>
                <tr>
                    <td><?= h($sensors->id) ?></td>
                    <td><?= h($sensors->name) ?></td>
                    <td><?= h($sensors->location_x) ?></td>
                    <td><?= h($sensors->location_y) ?></td>
                    <td><?= h($sensors->event_id) ?></td>
                    <td><?= h($sensors->created) ?></td>
                    <td><?= h($sensors->modified) ?></td>
                    <td><?= h($sensors->hall_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'Sensors', 'action' => 'view', $sensors->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'Sensors', 'action' => 'edit', $sensors->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'Sensors', 'action' => 'delete', $sensors->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sensors->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>

<script>
    var SalesChart = (function() {

        // Variables

        var $chart = $('#chart-sales-dark');


        // Methods

        function init($this) {
            var salesChart = new Chart($this, {
                type: 'line',
                options: {
                    scales: {
                        yAxes: [{
                            gridLines: {
                                color: Charts.colors.gray[700],
                                zeroLineColor: Charts.colors.gray[700]
                            },
                            ticks: {

                            }
                        }]
                    }
                },
                data: {
                    labels: ['May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    datasets: [{
                        label: 'Performance',
                        data: [0, 20, 10, 30, 15, 40, 20, 60, 60]
                    }]
                }
            });

            // Save to jQuery object

            $this.data('chart', salesChart);

        };


        // Events

        if ($chart.length) {
            init($chart);
        }

    })();
</script>
