<?php
//pr($hall->floor_image);
$size = getimagesize($hall->floor_image);

$imageH = $size[1];
$imageW = $size[0];

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<canvas id="Canvas" width="<?=$imageW?>" height="<?=$imageH?>" style="
    display: block;
    width: <?=$imageW?>px!important;
    height: <?=$imageH?>px!important;
    "></canvas>




<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Field</th>
        <th scope="col">Static</th>


    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Count of sensores</td>
        <td><?php echo count($sensores->toArray());?></td>
    </tr>

    <tr>
        <td>Count Of Total points </td>
        <td><?php echo count($sensores->toArray());?></td>
    </tr>

    <tr>
        <td>Average Points </td>
        <td><?php echo count($sensores->toArray());?></td>
    </tr>

    </tbody>
</table>



<script>


    var widthsOfFloor = <?=$imageW?>;
    var HeightOfFloor = <?=$imageH?>;
    var pixelToCMW = widthsOfFloor / widthsOfFloor;
    var pixelToCMH = HeightOfFloor / HeightOfFloor;
    console.log(pixelToCMH);
    console.log(pixelToCMW);
    var canvas = document.getElementById('Canvas');
    var context = canvas.getContext("2d");

    // Map sprite
    var mapSprite = new Image();
    mapSprite.src = "<?=$hall->floor_image?>";
    var counter = 1 ;
    var Marker = function () {
        this.Sprite = new Image();
        this.Sprite.src = "http://www.clker.com/cliparts/w/O/e/P/x/i/map-marker-hi.png"
        this.Width = 12;
        this.Height = 20;
        this.XPos = 0;
        this.YPos = 0;
        this.WPos = 0;
        this.HPos = 0;
        this.id = counter;
        counter++;
    }

    function changeMarker(target,xValue,yValue){
        Markers.forEach(function(marker,index){
            if(marker.id == target){
                Markers[index].XPos = xValue;
                Markers[index].YPos = yValue;
            }
        })
    }

    var Markers = new Array();

    var mouseClicked = function (mouse) {
        // Get corrent mouse coords
        var rect = canvas.getBoundingClientRect();
        var mouseXPos = parseInt( Math.abs(mouse.location_x / 100 ))
        var mouseYPos = parseInt( Math.abs(mouse.location_y / 100))


        // Move the marker when placed to a better location
        var marker = new Marker();
        marker.XPos = (mouseXPos - (marker.Width / 2)).toFixed(2);
        marker.YPos = (mouseYPos - marker.Height).toFixed(2);
        marker.WPos = marker.XPos ;
        marker.HPos = marker.YPos ;
        marker.title = mouse.name;

        Markers.push(marker);
        console.log(marker);

    }

    // Add mouse click event listener to canvas
    // canvas.addEventListener("mousedown", mouseClicked, false);
    setTimeout(function(){
        $jsonData = JSON.parse('<?=json_encode($sensores)?>');
        $jsonData.forEach(function(row){
            // console.log(row);
            setTimeout(function(){
                mouseClicked(row);
                // console.log(row);
            },500);
        })
    },1500);

    var firstLoad = function () {
        context.font = "15px Georgia";
        context.textAlign = "center";
    }

    firstLoad();

    var main = function () {
        draw();
    };

    var draw = function () {
        // Clear Canvas
        context.fillStyle = "#000";
        context.fillRect(0, 0, canvas.width, canvas.height);

        // Draw map
        // Sprite, X location, Y location, Image width, Image height
        // You can leave the image height and width off, if you do it will draw the image at default size
        context.drawImage(mapSprite, 0, 0, canvas.width, canvas.height);

        // Draw markers
        for (var i = 0; i < Markers.length; i++) {
            var tempMarker = Markers[i];
            // Draw marker
            context.drawImage(tempMarker.Sprite, tempMarker.XPos, tempMarker.YPos, tempMarker.Width, tempMarker.Height);

            // Calculate postion text
            var markerText = tempMarker.title;

            // Draw a simple box so you can see the position
            var textMeasurements = context.measureText(markerText);
            context.fillStyle = "#666";
            context.globalAlpha = 0.7;
            context.fillRect(tempMarker.XPos - (textMeasurements.width / 2), tempMarker.YPos - 15, textMeasurements.width, 20);
            context.globalAlpha = 1;

            // Draw position above
            context.fillStyle = "#000";
            context.fillText(markerText, tempMarker.XPos, tempMarker.YPos);
        }
    };

    setInterval(main, (1000 / 60));
</script>





