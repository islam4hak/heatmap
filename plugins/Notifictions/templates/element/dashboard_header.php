<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <span class="dropdown-item dropdown-header"><?=count($Notifications_list_array->toArray());?> <?=__('Notifications');?></span>
    <div class="dropdown-divider"></div>
    <?php
        foreach ($Notifications_list_array->toArray() as $note):
    ?>
    <a href="#" class="modal-opener dropdown-item" data-url="<?=ROOT_URL?>notifictions/notifications/view/<?=$note->id?>" class="dropdown-item">
        <i class="fas fa-envelope mr-2"></i> <?=$note->title?>
        <span class="float-right text-muted text-sm"><?=$note->created->timeAgoInWords()?></span>
    </a>
    <div class="dropdown-divider"></div>
    <?php endforeach; ?>
    <a href="<?=ROOT_URL?>notifictions/notifications/list_my_note" class="dropdown-item dropdown-footer"><?=__('See All Notifications');?></a>
</div>



<?php
$this->Html->scriptStart(['block' => 'scriptcode']);
?>
$('.modal-opener').click(function(){
var url = $(this).data("url");
console.log(url);
$.ajax({
type: "GET",
url: url,
dataType: 'text',
success: function(res) {
$('.modal-body').html(res);
// show modal
$('#myModal').modal('show');

},
error:function(request, status, error) {
console.log("ajax call went wrong:" + request.responseText);
}
});
});
<?php
$this->Html->scriptEnd();
?>
