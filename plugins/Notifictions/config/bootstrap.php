<?php
declare(strict_types=1);
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Notifictions\Event\NotificationListener;
$noteListner = new NotificationListener();
//EventManager::instance()->on($noteListner );
TableRegistry::getTableLocator()->get('Notifictions.Notifications')
    ->getEventManager()
    ->on($noteListner);

TableRegistry::getTableLocator()->get('Orders')
    ->getEventManager()
    ->on('Model.afterSave', function($event, $entity)
    {
        $noteListner = new NotificationListener();
        $noteListner->OrdersAfterSave($event,$entity);
    });

//TableRegistry::getTableLocator()->get('UsersManager.Users')
//    ->getEventManager()
//    ->on('Model.afterSave', function($event, $entity)
//    {
//        $noteListner = new NotificationListener();
//        $noteListner->afterRegister($event,$entity);
//
//    });



EventManager::instance()->on(
    'Model.Users.afterRegister',
    function ($event,$entity){
        $noteListner = new NotificationListener();
        $noteListner->afterRegister($event,$entity);

    }
);

    EventManager::instance()->on(
    'Model.Users.passwordReset',
    function ($event,$entity){
        $noteListner = new NotificationListener();
        $noteListner->resetPassword($event,$entity);

    }
);
