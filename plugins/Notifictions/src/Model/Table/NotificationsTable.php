<?php
declare(strict_types=1);

namespace Notifictions\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Notifications Model
 *
 * @property \Notifictions\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \Notifictions\Model\Entity\Notification newEmptyEntity()
 * @method \Notifictions\Model\Entity\Notification newEntity(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification[] newEntities(array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification get($primaryKey, $options = [])
 * @method \Notifictions\Model\Entity\Notification findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \Notifictions\Model\Entity\Notification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \Notifictions\Model\Entity\Notification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Notification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \Notifictions\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class NotificationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('notifications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Notifictions.Users',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
//        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function readIt($id){
        $note = $this->get($id);
        $note->set('red' , 1);
        $this->save($note);
    }
    public function oldIt($id){
        $note = $this->get($id);
        $note->set('new' , 0);
        $this->save($note);
    }
}
