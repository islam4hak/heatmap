<?php
declare(strict_types=1);

namespace Notifictions\Model\Entity;

use Cake\ORM\Entity;

/**
 * Notification Entity
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $body
 * @property string|null $model
 * @property int|null $user_id
 * @property bool $send_email
 * @property bool $send_phone
 * @property string|null $params
 * @property bool $red
 * @property bool $new
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Notifictions\Model\Entity\User $user
 */
class Notification extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'body' => true,
        'model' => true,
        'user_id' => true,
        'send_email' => true,
        'send_phone' => true,
        'params' => true,
        'red' => true,
        'new' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
    ];
}
