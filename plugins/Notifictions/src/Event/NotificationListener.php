<?php
namespace Notifictions\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\ORM\TableRegistry;


class NotificationListener implements EventListenerInterface
{
    public function implementedEvents() :array
    {
        return [
            'Model.Users.afterRegister' => 'afterRegister',
            'Model.Users.afterRemove' => 'afterRemove',
            'Model.Orders.afterSave' => 'OrdersAfterSave',
            'Model.afterSave' => 'NotifictionAfterSave',
        ];
    }

    public function OrdersAfterSave($event, $order){
        $noteModel = TableRegistry::getTableLocator()->get('Notifications');
        $entity = $noteModel->newEmptyEntity();
        $entity->title = "New Order";
        $entity->body = "PLease check this new Order #".$order->id." you can find it at this <a href='".ROOT_URL."/orders/view/".$order->id."'>link</a> ";
        $entity->user_id = 1;
        $entity->new = 1;
        $entity->send_email = 1;
        $entity->send_phone = 1;
        $entity->red = 1;
        if($noteModel->save($entity)){
//            echo "Testing ";
        }else{
//            pr($entity);
        }
//        var_dump($entity);
//        die();

    }
    public function afterRegister($event, $user)
    {

        $activationCode = $user->login_token;
        $mailer = new Mailer('default');
        $mailer->setFrom(['info@thecourierexperts.com' => 'The Courier Experts'])
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject('Registration')
            ->viewBuilder()
            ->setTemplate('verification')
            ->setVars(
                [
                    'activationCode' => $activationCode ,
                    'callToaction' => ['url'=>ROOT_URL.'/users-manager/users/verify/'.$activationCode,'text'=>'Verify Your Account Now ›'],
                    'main_title'=>'Thank <span style="font-size: 38px; color: #fc7318;">You</span>',
                    'sub_title' => 'You Have Successfully Registered'
                ]
            );


//        $mailer->deliver();


    }

    public function resetPassword($event, $user)
    {
        $activationCode = $user->login_token;
        $mailer = new Mailer('default');
        $mailer->setFrom(['info@thecourierexperts.com' => 'The Courier Experts'])
            ->setTo($user->email)
            ->setEmailFormat('html')
            ->setSubject('Password reset')
            ->viewBuilder()
            ->setTemplate('reset')
            ->setVars(
                [
                    'activationCode' => $activationCode ,
                    'callToaction' => ['url'=>ROOT_URL.'/users-manager/users/resetpass/'.$activationCode,'text'=>'Reset Your Password Now ›'],
                    'main_title'=>'Password <span style="font-size: 38px; color: #fc7318;">Reset</span>',
                    'sub_title' => 'Please follow this email to reset your password',
                    'userData' => $user
                ]
            );


        $mailer->deliver();


    }

    public function afterRemove($event, $user)
    {
        Log::write('debug', $user['name'] . ' has deleted his/her account.');
    }

    public function NotifictionAfterSave(Event $event , $note){
        // the logic for this event
        if($note->send_email == 1){
//            // send the email for this notification
//            $mailer = new Mailer('default');
////            var_dump($mailer);
////            die();
//
//            //check on live servers
//            try {
//                $mailer->setFrom(['sadacode.com@gmail.com' => PROJECT_NAME ])
//                    ->setTo('islam4hak@gmail.com')
//                    ->setSubject('About')
//                    ->deliver('My message');
//            } catch (\Exception $exception){
//                die($exception);
//            }

        }
    }


}
?>
