<?php
declare(strict_types=1);

namespace Notifictions\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Notifications component
 */
class NotificationsHandlerComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    function setNote(){
        if(isset($this->getController()->Auth->user()['id'] )){
            $notes = TableRegistry::getTableLocator()->get('Notifications');
            $list = $notes->find('all')->where([
                'OR' => [
                    'user_id' => $this->getController()->Auth->user()['id']  , 'user_id IS' => null
                ]
            ] )->where( ['new'=>1]) ;
            $this->getController()->set('Notifications_list_array',$list);
        }
    }


}
