<?php
declare(strict_types=1);

namespace Notifictions\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Notifictions\Controller\Component\NotificationsComponent;

/**
 * Notifictions\Controller\Component\NotificationsComponent Test Case
 */
class NotificationsComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Notifictions\Controller\Component\NotificationsComponent
     */
    protected $Notifications;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Notifications = new NotificationsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Notifications);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
