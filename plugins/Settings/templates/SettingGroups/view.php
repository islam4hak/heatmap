<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $settingGroup
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('Edit Setting Group'), ['action' => 'edit', $settingGroup->id], ['class' => 'nav-link']) ?></li>
<li><?= $this->Form->postLink(__('Delete Setting Group'), ['action' => 'delete', $settingGroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $settingGroup->id), 'class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Setting Groups'), ['action' => 'index'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('New Setting Group'), ['action' => 'add'], ['class' => 'nav-link']) ?> </li>
<li><?= $this->Html->link(__('List Parent Setting Groups'), ['controller' => 'SettingGroups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Parent Setting Group'), ['controller' => 'SettingGroups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Setting Fields'), ['controller' => 'SettingFields', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Setting Field'), ['controller' => 'SettingFields', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Child Setting Groups'), ['controller' => 'SettingGroups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Child Setting Group'), ['controller' => 'SettingGroups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="settingGroups view large-9 medium-8 columns content">
    <h3><?= h($settingGroup->name) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($settingGroup->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Parent Setting Group') ?></th>
                <td><?= $settingGroup->has('parent_setting_group') ? $this->Html->link($settingGroup->parent_setting_group->name, ['controller' => 'SettingGroups', 'action' => 'view', $settingGroup->parent_setting_group->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Icon') ?></th>
                <td><?= h($settingGroup->icon) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($settingGroup->id) ?></td>
            </tr>
        </table>
    </div>
    <div class="row">
        <h4><?= __('Description Body') ?></h4>
        <?= $this->Text->autoParagraph(h($settingGroup->description_body)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Setting Fields') ?></h4>
        <?php if (!empty($settingGroup->setting_fields)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Type') ?></th>
                    <th scope="col"><?= __('Stored Val') ?></th>
                    <th scope="col"><?= __('Setting Group Id') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($settingGroup->setting_fields as $settingFields): ?>
                <tr>
                    <td><?= h($settingFields->id) ?></td>
                    <td><?= h($settingFields->name) ?></td>
                    <td><?= h($settingFields->description) ?></td>
                    <td><?= h($settingFields->type) ?></td>
                    <td><?= h($settingFields->stored_val) ?></td>
                    <td><?= h($settingFields->setting_group_id) ?></td>
                    <td><?= h($settingFields->created) ?></td>
                    <td><?= h($settingFields->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'SettingFields', 'action' => 'view', $settingFields->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'SettingFields', 'action' => 'edit', $settingFields->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'SettingFields', 'action' => 'delete', $settingFields->id], ['confirm' => __('Are you sure you want to delete # {0}?', $settingFields->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Setting Groups') ?></h4>
        <?php if (!empty($settingGroup->child_setting_groups)): ?>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Name') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Description Body') ?></th>
                    <th scope="col"><?= __('Icon') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($settingGroup->child_setting_groups as $childSettingGroups): ?>
                <tr>
                    <td><?= h($childSettingGroups->id) ?></td>
                    <td><?= h($childSettingGroups->name) ?></td>
                    <td><?= h($childSettingGroups->parent_id) ?></td>
                    <td><?= h($childSettingGroups->description_body) ?></td>
                    <td><?= h($childSettingGroups->icon) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['controller' => 'SettingGroups', 'action' => 'view', $childSettingGroups->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Html->link(__('Edit'), ['controller' => 'SettingGroups', 'action' => 'edit', $childSettingGroups->id], ['class' => 'btn btn-secondary']) ?>
                        <?= $this->Form->postLink( __('Delete'), ['controller' => 'SettingGroups', 'action' => 'delete', $childSettingGroups->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childSettingGroups->id), 'class' => 'btn btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
    </div>
</div>
