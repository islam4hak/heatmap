<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $settingField
 * @var \App\Model\Entity\SettingGroup[]|\Cake\Collection\CollectionInterface $settingGroups
 */
?>
<?php $this->extend('/layout/TwitterBootstrap/dashboard'); ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(__('List Setting Fields'), ['action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('List Setting Groups'), ['controller' => 'SettingGroups', 'action' => 'index'], ['class' => 'nav-link']) ?></li>
<li><?= $this->Html->link(__('New Setting Group'), ['controller' => 'SettingGroups', 'action' => 'add'], ['class' => 'nav-link']) ?></li>
<?php $this->end(); ?>
<?php $this->assign('tb_sidebar', '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>'); ?>

<div class="settingFields form content">
    <?= $this->Form->create($settingField) ?>
    <fieldset>
        <legend><?= __('Add Setting Field') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('type',['options'=>TYPES_CUSTOM_FIELD]);
            echo $this->Form->control('setting_group_id', ['options' => $settingGroups]);
            echo $this->Form->control('help_text',['type' =>'text']);
            echo $this->Form->control('placeholder');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
