<?php
declare(strict_types=1);

namespace Settings\Model\Entity;

use Cake\ORM\Entity;

/**
 * SettingGroup Entity
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property string $description_body
 * @property string $icon
 *
 * @property \Settings\Model\Entity\SettingGroup $parent_setting_group
 * @property \Settings\Model\Entity\SettingField[] $setting_fields
 * @property \Settings\Model\Entity\SettingGroup[] $child_setting_groups
 */
class SettingGroup extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'parent_id' => true,
        'description_body' => true,
        'icon' => true,
        'parent_setting_group' => true,
        'setting_fields' => true,
        'child_setting_groups' => true,
    ];
}
