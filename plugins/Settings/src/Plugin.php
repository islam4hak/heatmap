<?php
declare(strict_types=1);

namespace Settings;

use Cake\Core\BasePlugin;
use Cake\Core\PluginApplicationInterface;
use Cake\Http\MiddlewareQueue;
use Cake\ORM\TableRegistry;
use Cake\Routing\RouteBuilder;

/**
 * Plugin for Settings
 */
class Plugin extends BasePlugin
{
    /**
     * Load all the plugin configuration and bootstrap logic.
     *
     * The host application is provided as an argument. This allows you to load
     * additional plugin dependencies, or attach events.
     *
     * @param \Cake\Core\PluginApplicationInterface $app The host application
     * @return void
     */
    public function bootstrap(PluginApplicationInterface $app): void
    {
        parent::bootstrap($app);
        $this->SettingsStartUp();

    }
    private function keyH($key){
        $key = str_replace(' ', '_',$key);
        $key = strtolower($key);
        return $key;
    }

    private function SettingsStartUp(){
        $settings=[];
        $settingModel = TableRegistry::getTableLocator()->get('Settings.SettingGroups');
        $allGroup = $settingModel->find('all')->where(['parent_id' =>0])->contain(['ChildSettingGroups.SettingFields'])->all();
        foreach ($allGroup as $group){
            $mainKey = $this->keyH($group->name);
            $secondKey = [];
            if(is_array($group->child_setting_groups)){
                foreach ($group->child_setting_groups as $sub){
                    $final = [];
                    if(is_array($sub->setting_fields)){

                        foreach ($sub->setting_fields as $field){
                            $fieldKey = $this->keyH($field->name);
                            $val = $field->stored_val;
                            $final[$fieldKey] = $val;
                        }
                    }
                    $secondKey[ $this->keyH($sub->name)]=$final;
                }
            }
            $settings[$mainKey]=$secondKey;
        }
        define('SETTING_KEY',$settings);
    }

    /**
     * Add routes for the plugin.
     *
     * If your plugin has many routes and you would like to isolate them into a separate file,
     * you can create `$plugin/config/routes.php` and delete this method.
     *
     * @param \Cake\Routing\RouteBuilder $routes The route builder to update.
     * @return void
     */
    public function routes(RouteBuilder $routes): void
    {
        $routes->plugin(
            'Settings',
            ['path' => '/settings'],
            function (RouteBuilder $builder) {
                // Add custom routes here

                $builder->fallbacks();
            }
        );
        parent::routes($routes);
    }

    /**
     * Add middleware for the plugin.
     *
     * @param \Cake\Http\MiddlewareQueue $middleware The middleware queue to update.
     * @return \Cake\Http\MiddlewareQueue
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        // Add your middlewares here

        return $middlewareQueue;
    }
}
