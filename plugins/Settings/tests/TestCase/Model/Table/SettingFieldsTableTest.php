<?php
declare(strict_types=1);

namespace Settings\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Settings\Model\Table\SettingFieldsTable;

/**
 * Settings\Model\Table\SettingFieldsTable Test Case
 */
class SettingFieldsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Settings\Model\Table\SettingFieldsTable
     */
    protected $SettingFields;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'plugin.Settings.SettingFields',
        'plugin.Settings.SettingGroups',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SettingFields') ? [] : ['className' => SettingFieldsTable::class];
        $this->SettingFields = TableRegistry::getTableLocator()->get('SettingFields', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SettingFields);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
